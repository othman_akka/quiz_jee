package com.models;

import java.io.Serializable;


public class User implements Serializable{
	private String user;
	private String pass;
	private String email;
	
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public User(String user, String pass) {
		super();
		this.user = user;
		this.pass = pass;
	}

	public User(String user, String pass, String email) {
		super();
		this.user = user;
		this.pass = pass;
		this.email = email;
	}

	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


}
