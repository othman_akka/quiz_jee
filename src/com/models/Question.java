package com.models;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Question implements Serializable {
	private int ID;
	private String theme;
	private String qst;
	private String ch1;
	private String ch2;
	private String ch3;
	private String reponse;
	private String lineQ;
    
	private List<Question> questions=new ArrayList<>();
	
	public Question() {
	   questions=new ArrayList<>();
	}
	public Question(String theme, String qst, String ch1, String ch2, String ch3, String reponse) {
		this.theme = theme;
		this.qst = qst;
		this.ch1 = ch1;
		this.ch2 = ch2;
		this.ch3 = ch3;
		this.reponse = reponse;
	}
	public Question(int ID,String theme, String qst, String ch1, String ch2, String ch3, String reponse) {
		this.ID=ID;
		this.theme = theme;
		this.qst = qst;
		this.ch1 = ch1;
		this.ch2 = ch2;
		this.ch3 = ch3;
		this.reponse = reponse;
	}

	public List<Question> traitementFichier(String path)
	{  
		int pipe1,pipe2;
		int vir1,vir2,vir3;
		
		try {
			BufferedReader flot=new BufferedReader(new FileReader(path));
			while((theme=flot.readLine())!=null&&(lineQ=flot.readLine())!=null)
                        {
                         pipe1 = lineQ.indexOf('|');
                         pipe2 =lineQ.lastIndexOf('|');
                         vir1 = lineQ.indexOf(',');
                         vir2 = lineQ.lastIndexOf(',');
                         qst = lineQ.substring(0, pipe1);
                         ch1 = lineQ.substring(pipe1+1,vir1);
                         ch2 = lineQ.substring(vir1+1,vir2);
                         ch3 = lineQ.substring(vir2+1,pipe2);
                         reponse = lineQ.substring(pipe2+1);
                         questions.add(new Question(theme,qst,ch1, ch2,ch3,reponse));
                        }
			return questions;
		}catch (Exception e) {
			
		}
		return null;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getQst() {
		return qst;
	}

	public void setQst(String qst) {
		this.qst = qst;
	}

	public String getCh1() {
		return ch1;
	}

	public void setCh1(String ch1) {
		this.ch1 = ch1;
	}

	public String getCh2() {
		return ch2;
	}

	public void setCh2(String ch2) {
		this.ch2 = ch2;
	}

	public String getCh3() {
		return ch3;
	}

	public void setCh3(String ch3) {
		this.ch3 = ch3;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}

}
