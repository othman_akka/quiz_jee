package com.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.Element;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.catalina.valves.rewrite.Substitution.StaticElement;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;

import com.bdd.ConnectionBd;
import com.bdd.FichierBDD;
import com.models.Question;
import com.models.User;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;


@WebServlet("/Authentification")
public class Authentification extends HttpServlet {
	private static final long serialVersionUID = 1L;
	FichierBDD fichier;
	private List<Question>questions;
	Question question;
	private static int j=0;
	private int score=0;
	private Date aujourdhui;
	private DateFormat mediumDateFormatEN ;
	private int i;
	private ConnectionBd bd;
	private static String questionBDD;
	private ArrayList<String>listURL;
	private int id=0;
	static int index=0;
	static int cpt=0;
 
    public Authentification() {
    	fichier=new FichierBDD();
    	questions=new ArrayList<Question>();
    	question=new Question();
    	bd=new ConnectionBd();
    	listURL=new ArrayList<>();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		   this.getServletContext().getRequestDispatcher("/WEB-INF/authentification.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	    String name=request.getParameter("user");
	    String pass=request.getParameter("pass");
	    
	    ConnectionBd cnx=new ConnectionBd();
	    User user =new User(name,pass);
	    ///
	    String teste=request.getParameter("teste");
	    String action=request.getParameter("action");
	    String choix=request.getParameter("connect"); 
	    
	    String userIn=request.getParameter("userIn");
	    String passIn=request.getParameter("passIn");
	    String mail=request.getParameter("mail");
	    String passInR=request.getParameter("passInR");
	    
	    
	    questionBDD=request.getParameter("choix");
	    String BDD=request.getParameter("choix2");
	    String finTest=request.getParameter("lien");
	    String typeTest=request.getParameter("valider");
        String choixReponse=request.getParameter("radioname");
        
        String inscre=request.getParameter("inscre");
         
        System.out.println(BDD);
        
	     if(choix !=null)
	     {
	    	if(choix.equals("SigIn")) 
	    	{
	             if(cnx.verifierLogin(name,pass).equals(name) && name.equals("admin"))
	              {
	    	        this.getServletContext().getRequestDispatcher("/WEB-INF/possibiliteAdmin.jsp").forward(request, response);
	               }
	              else if(cnx.verifierLogin(name,pass).equals(name) && !name.equals("admin"))
	              {
	            	  request.setAttribute("user",name);
	            	  this.getServletContext().getRequestDispatcher("/WEB-INF/choixClientExam.jsp").forward(request, response);
	            	  
	              }
	              else
	            	  this.getServletContext().getRequestDispatcher("/WEB-INF/authentification.jsp").forward(request, response);
	              
	         }else if(choix.equals("Annuler"))
	         {
	        	 this.getServletContext().getRequestDispatcher("/WEB-INF/authentification.jsp").forward(request, response);
	         }
	         else if(choix.equals("free"))
	         {
	        	 request.getRequestDispatcher("/WEB-INF/formInscription.jsp").forward(request, response);
	         }
	      }
	     else if(inscre!=null && inscre.equals("Ajouter"))
	      {
	    	  if(passIn.equals(passInR)) {
	    	  User user2=new User(userIn,passIn,mail);  
	    	  bd.Ajouter(user2);
	    	  System.out.println("ok");
	    	  this.getServletContext().getRequestDispatcher("/WEB-INF/authentification.jsp").forward(request, response);
	    	  }
	    	  else
	    	  {
	    		  request.getRequestDispatcher("/WEB-INF/formInscription.jsp").forward(request, response); 
	    	  }
	      }else if(inscre!=null && inscre.equals("Annuler"))
	      {
	    	  this.getServletContext().getRequestDispatcher("/WEB-INF/authentification.jsp").forward(request, response);
	      }
	     else if(action !=null) 
	     {
	       if(action.equals("Base")) 
	       {
	         request.getRequestDispatcher("/WEB-INF/choixBaseDonne.jsp").forward(request, response);
	       }
	         else if(action.equals("Quitter"))
	       {
	       request.getRequestDispatcher("/WEB-INF/authentification.jsp").forward(request, response);;
	       }
	       else if(action.equals("Tester"))
	       {
		   request.getRequestDispatcher("/WEB-INF/choixTest.jsp").forward(request, response);
	        }
	    }
       else if(finTest!=null && finTest.equals("quitter"))
	    {
	    	aujourdhui=null;
	        mediumDateFormatEN =null;
	    	 this.getServletContext().getRequestDispatcher("/WEB-INF/authentification.jsp").forward(request, response);
	    }
	    else if(BDD!=null &&BDD.equals("valide"))
	    { 
	    	
	    	if(!questionBDD.equals("")) {

	    		Vector<Integer>listID=new Vector<>();
	    	    listID=bd.listID();
	    	    if(listID.size()==0)
	    	    {
	    	    	id++;
	    	    }
	    	    else {
	    	    for(int j=2;j<listID.size()+10;j++)
	    	    {
	    	    	
	    	    	int i=0;
	    	    	while (i<listID.size())
	    	    	{
	    	    		if(listID.get(i).intValue()!=j)
	    	    		{
	    	    		   id=j;
	    	    		   index=1;
	    	    		   break;
	    	    		}
	    	    		i++;
	    	    	}
	    	    	if(index==1)
	    	    	{
	    	    		break;
	    	    	}
	    	    }
	    	    }
	    	    listURL.add(questionBDD);
	    	    
	    		for(int i=0;i<listURL.size();i++)
	    		{
	    			if(questionBDD.equals(listURL.get(i))) {
	    				
	    				  if(fichier.deletAllQuestion()) {
	    		    	      questions=question.traitementFichier(questionBDD);
	    		    	    if(questions.size()!=0)
	    		    	    {
	    		    	     for ( i=0; i <questions.size(); i++)
	    		    	     {
	    		    		   fichier.AjouterQuestion(id,questions.get(i));
	    		    	     }
	    		    	     request.getRequestDispatcher("/WEB-INF/choixBaseDonne.jsp").forward(request, response);
	    		    	     break;
	    		    	    }
	    		    	}
	    			}
	    			else {
	    			        questions=question.traitementFichier(questionBDD);
	    		    	    if(questions.size()!=0)
	    		    	    {
	    		    	     for ( i=0; i <questions.size(); i++)
	    		    	     {
	    		    	    	
	    		    		   fichier.AjouterQuestion(id,questions.get(i));
	    		    		  
	    		    	     }
	    		    	     request.getRequestDispatcher("/WEB-INF/choixBaseDonne.jsp").forward(request, response);
	    		    	     break;
	    		    	    }
	    			}
	    		}
	    		
	    		
	    		
	    	  /*if(fichier.deletAllQuestion()) {
	    	      questions=question.traitementFichier(questionBDD);
	    	    if(questions.size()!=0)
	    	    {
	    	     for ( i=0; i <questions.size(); i++)
	    	     {
	    		   fichier.AjouterQuestion(id,questions.get(i));
	    		  
	    	     }
	    	     request.getRequestDispatcher("/WEB-INF/choixBaseDonne.jsp").forward(request, response);
	    	    
	    	}
	    	}else {
	    		request.getRequestDispatcher("/WEB-INF/choixBaseDonne.jsp").forward(request, response);
	    	}*/
	    	}///////
	    }	
	   else if(BDD !=null && BDD.equals("quiter"))
	   {
		   this.getServletContext().getRequestDispatcher("/WEB-INF/possibiliteAdmin.jsp").forward(request, response);
	   }
	    else if(typeTest!=null)
	    {  
	    	
	    	if(typeTest.equals("Valider"))
	    	{  
	    		
	    		questions.clear();
	    		questions=fichier.listQuestion();
	    		aujourdhui=new Date();
	            mediumDateFormatEN = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,DateFormat.MEDIUM, new Locale("EN","en"));
	    		
	    			if(choixReponse!=null &&choixReponse.equals(questions.get(j-1).getReponse()))
	    			{
	    				score++;	
	    			}
	    		
	    	     request.setAttribute("size",questions.size());
	    	     
	    	     if(cpt==0) {
	    		if(j<questions.size()) {
	    		     while(j<questions.size())
	    		       {   
	    		        	question=questions.get(j);
	    		        	request.setAttribute("j",j+1);
	    	                request.setAttribute("q",question);
	    	                request.getRequestDispatcher("/WEB-INF/testQCM.jsp").forward(request, response);
	    	       
	    	                j++;
	    	                break;
	    		         }
	    		}else
	    		{
	    			request.setAttribute("date", mediumDateFormatEN .format(aujourdhui));
	    			request.setAttribute("questions", j);
	    			HttpSession session = request.getSession() ;
	    			session.setAttribute("score", score) ;
	    			request.getRequestDispatcher("/WEB-INF/score.jsp").forward(request, response);
	    		}
	    	     }else
	    	     {
	 	    		if(j<questions.size()) {
		    		     while(j<questions.size())
		    		       {   
		    		        	question=questions.get(j);
		    		        	request.setAttribute("j",j+1);
		    	                request.setAttribute("q",question);
		    	                request.getRequestDispatcher("/WEB-INF/testQCM2.jsp").forward(request, response);
		    	       
		    	                j++;
		    	                break;
		    		         }
		    		}else
		    		{
		    			request.setAttribute("date", mediumDateFormatEN .format(aujourdhui));
		    			request.setAttribute("questions", j);
		    			HttpSession session = request.getSession() ;
		    			session.setAttribute("score", score) ;
		    			request.getRequestDispatcher("/WEB-INF/score.jsp").forward(request, response);
		    		} 
	    	     }
	    	}
	    	else if(typeTest.equals("quitter"))
	    	{
	    		 this.getServletContext().getRequestDispatcher("/WEB-INF/possibiliteAdmin.jsp").forward(request, response);
	    	}
	    		
	    }

	}

}
