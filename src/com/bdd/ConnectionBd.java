package com.bdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

import com.models.User;

public class ConnectionBd {
       private Connection cnx;
       private Statement state;
       private ResultSet res;
       
       
       public ConnectionBd() {
	
	}

	public String verifierLogin(String user,String pass)
       {
       
    	   try{
    		    state=null;
    			state=cnx();
    		    res=state.executeQuery("select user,pass from login");
    		   while(res.next()) {
    		   if(user.equals(res.getString("user")) && pass.equals(res.getString("pass"))) {
    			   return res.getString("user");
    		   }
    		   }
    	   }catch (Exception e) {
    		   e.getMessage();
    	   }finally {
    		   try {
    		   if(res!=null)
    			   res.close();
    		   if(cnx!=null)
    			   cnx.close();
    		   if(state!=null)
    			   state.close();
    		   }catch (Exception e) {
				
			}
    	   }
    	   
    	  return "";
       }
	public Vector<Integer> listID()
    {
		Vector<Integer>listID=new Vector<>();
        
 	   try{
 		    state=null;
 			state=cnx();
 		    res=state.executeQuery("select distinct ID from fichiertab");
 		   while(res.next()) {
 		       listID.add(res.getInt("ID"));
 		   }
 		   return listID;
 	   }catch (Exception e) {
 		   e.getMessage();
 	   }finally {
 		   try {
 		   if(res!=null)
 			   res.close();
 		   if(cnx!=null)
 			   cnx.close();
 		   if(state!=null)
 			   state.close();
 		   }catch (Exception e) {
				
			}
 	   }
 	   
 	  return null;
    }
   public void Ajouter(User userIn) {
	   try{
		   state=null;
		   state=cnx();
		   state.executeUpdate("insert into login values('"+userIn.getUser()+"','"+userIn.getPass()+"','"+userIn.getEmail()+"')");
		
	   }catch (Exception e) {
		   e.getMessage();
	   }finally {
		   try {
		   if(cnx!=null)
			   cnx.close();
		   if(state!=null)
			   state.close();
		   }catch (Exception e) {
			
		}
	   }
	   
   }
   // Acces to BDD
   public Statement cnx()
   {    try {
	        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
	        cnx=DriverManager.getConnection("jdbc:mysql://localhost:3306/quiz?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root","");
	        return   state=cnx.createStatement();
            }catch (Exception e) {
	           e.getMessage();
            }
       return null;
   }

}
