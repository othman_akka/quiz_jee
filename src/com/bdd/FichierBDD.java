package com.bdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.models.Question;

public class FichierBDD {
	  private Connection cnx;
      private Statement state;
      private ResultSet res;
      private Question question;
      private List<Question> questions;
      public FichierBDD() {
    		question=new Question();
    		questions=new ArrayList<>();
  	}

      
      //add qustion in BDD
     public boolean AjouterQuestion(int id ,Question question) {
  	   try{
  		   state=cnx();
  		   state.executeUpdate("insert into fichierTab values("+id+",'"+question.getTheme()+"','"+question.getQst()+"','"+question.getCh1()+"','"+question.getCh2()+"','"+question.getCh3()+"','"+question.getReponse()+"')");
  		return true;
  	   }catch (Exception e) {
  		   e.getMessage();
  	   }finally {
  		   try {
  		   if(cnx!=null)
  			   cnx.close();
  		   if(state!=null)
  			   state.close();
  		   }catch (Exception e) {
  			
  		}
  	   }
  	   return false;
  	   
     }
     //liste qustions from BDD
    public List<Question> listQuestion() {
 	   try{
 		   state=cnx();
 		  res= state.executeQuery("select * from fichiertab");
 		   
 		   while(res.next())
 		   {
 			   questions.add(new Question(res.getString("theme"), res.getString("question"), res.getString("choix1"),res.getString("choix2"),res.getString("choix3"), res.getString("reponse")));
 		   }
 			   
 		return questions;
 	   }catch (Exception e) {
 		   e.getMessage();
 	   }finally {
 		   try {
 		   if(cnx!=null)
 			   cnx.close();
 		   if(state!=null)
 			   state.close();
 		   }catch (Exception e) {
 			
 		}
 	   }
 	   return null;
 	   
    }
    public boolean deletAllQuestion() {
  	   try{
  		   state=cnx();
  		   state.executeUpdate("delete from fichiertab ");
  		   return true;
  	   }catch (Exception e) {
  		   e.getMessage();
  	   }finally {
  		   try {
  		   if(cnx!=null)
  			   cnx.close();
  		   if(state!=null)
  			   state.close();
  		   }catch (Exception e) {
  			
  		}
  	   }
  	   return false;
  	   
     }
    // delete all fichier
    
     // Acces to BDD
     public Statement cnx()
     {    try {
  	        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
  	        cnx=DriverManager.getConnection("jdbc:mysql://localhost:3306/quiz?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root","");
  	        return   state=cnx.createStatement();
              }catch (Exception e) {
  	           e.getMessage();
              }
         return null;
     }

}
