-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 27 Janvier 2019 à 23:53
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `quiz`
--

-- --------------------------------------------------------

--
-- Structure de la table `fichiertab`
--

CREATE TABLE IF NOT EXISTS `fichiertab` (
  `ID` int(200) NOT NULL,
  `theme` varchar(200) NOT NULL,
  `question` varchar(200) NOT NULL,
  `choix1` varchar(200) NOT NULL,
  `choix2` varchar(200) NOT NULL,
  `choix3` varchar(200) NOT NULL,
  `reponse` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `fichiertab`
--

INSERT INTO `fichiertab` (`ID`, `theme`, `question`, `choix1`, `choix2`, `choix3`, `reponse`) VALUES
(1, 'la politique', 'Quel est le nom du président du gouvernementactuel ?', 'El Fassi', 'Ben Kiran', 'Chabat', '2'),
(1, 'math', 'quelle la valeur de 1+6 ?', '2', '5', '7', '3'),
(2, 'la politique', 'Quel est le nom du président du gouvernementactuel ?', 'El Fassi', 'Ben Kiran', 'Chabat', '2'),
(2, 'math', 'quelle la valeur de 1+6 ?', '2', '5', '7', '3'),
(2, 'la politique', 'Quel est le nom du président du gouvernementactuel ?', 'El Fassi', 'Ben Kiran', 'Chabat', '2'),
(2, 'math', 'quelle la valeur de 1+6 ?', '2', '5', '7', '3');

-- --------------------------------------------------------

--
-- Structure de la table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `user` varchar(200) NOT NULL,
  `pass` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `login`
--

INSERT INTO `login` (`user`, `pass`, `email`) VALUES
('admin', 'admin', 'admin.admin@gmail.com'),
('othman', 'akka', 'othman.aka@gmail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
